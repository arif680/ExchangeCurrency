# Currency Converter
Using Django framework as the backend, no specific frontend framework used except HTML, JS, CSS  

## Running dockerized Django app
Docker must be installed first
* clone this repository
* in terminal, go to root directory /ExchangeCurrency where `Dockerfile` and `docker-compose.yml` file exists 
* run with command `docker-compose up`
* In browser, open url `http://127.0.0.1:8000`, now the web application of Foreign Exchange Currency can be tested
