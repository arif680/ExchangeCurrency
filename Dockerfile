# This file is a template, and might need editing before it works on your project.
# Guide: https://www.youtube.com/watch?v=nh1ynJGJuT8&t=1065s
FROM python:3.8-alpine

ENV PATH="/scripts:${PATH}" 

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN pip install -r /requirements.txt
RUN apk del .tmp

RUN mkdir /app
COPY ./application /app
WORKDIR /app
COPY ./scripts /scripts

RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

RUN adduser -D user
RUN chown -R user:user /vol
RUN chmod -R 755 /vol/web 
USER user
# switch to user

CMD ["entrypoint.sh"]

